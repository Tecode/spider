# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://doc.scrapy.org/en/latest/topics/item-pipeline.html
import json
import scrapy
from scrapy.pipelines.images import ImagesPipeline
import time
import os
import sys
reload(sys) 
sys.setdefaultencoding('utf-8')
# 导入数据库模块
import pymysql
# 打开数据库连接
db = pymysql.Connect(
    host='localhost',
    port=3306,
    user='root',
    passwd='',
    db='site_db',
    charset='utf8'
)
# 使用cursor()方法获取操作游标 
cursor = db.cursor()

class ImagespiderPipeline(object):
    def __init__(self):
        self.file = open("E:\python\girl_info.json", 'w')
    def process_item(self, item, spider):
        content = json.dumps(dict(item), ensure_ascii = False) + ',\n'
        self.file.write(content)
        return item
    def close_spider(self, spider):
        self.file.close()

# 保存图片
class MyImagesPipeline(ImagesPipeline):
    def get_media_requests(self, item, info):
        header = {    
            'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8',
            'Accept-Encoding': 'gzip, deflate, sdch',
            'Accept-Language': 'en-US,en;q=0.8',
            'Cache-Control': 'max-age=0',
            'Host': 'img1.mm131.me',
            'If-Modified-Since': 'Mon, 15 Jan 2018 02:18:26 GMT',
            'If-None-Match': '"5a6ead54-5335"',
            'Proxy-Connection': 'keep-alive',
            'Referer': 'http://www.mm131.com/xinggan/',
            'Upgrade-Insecure-Requests': 1,
            'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.110 Safari/537.36',
    }
        yield scrapy.Request(item['img_url'], headers = header)
    def item_completed(self, results, item, info):
        image_paths = [x['path'] for ok, x in results if ok]
        # if not image_paths:
        #     raise DropItem("Item contains no images")
        # item['image_paths'] = image_paths
        return item

# 爬取站长素材的,保存图片
class FreePsdPipeline(ImagesPipeline):
    def get_media_requests(self, item, info):
        print (item['detail_img'])
        yield scrapy.Request(item['detail_img'])
    def item_completed(self, results, item, info):
        image_paths = [x['path'] for ok, x in results if ok]
    # SQL 插入语句 数据库相关,看个人的需求
        sql = "INSERT INTO site_psd_info(title, \
            image_url, link_url, local_url, download_url, description, type, date_time) \
            VALUES ('%s', '%s', '%s', '%s', '%s', '%s','%s', '%s')" % \
            (item['title'], item['img_url'], item['link_url'], image_paths[0][5:], item['download_url'], item['description'], item['type'], int(round(time.time() * 1000)))
        try:
        # 执行sql语句
            cursor.execute(sql)
            # 提交到数据库执行
            db.commit()
        except:
        # 如果发生错误则回滚
            db.rollback()
        print(results, image_paths, '---------------4545')
        if not image_paths:
            raise DropItem("Item contains no images")
        item['image_paths'] = image_paths
        return item
# 爬取HTML模板
class ChinaZHtmlPipeline(ImagesPipeline):
    def get_media_requests(self, item, info):
        print (item['detail_img'])
        yield scrapy.Request(item['detail_img'])
    def item_completed(self, results, item, info):
        image_paths = [x['path'] for ok, x in results if ok]
    # SQL 插入语句 数据库相关,看个人的需求
        sql = "INSERT INTO site_html_info(title, \
            image_url, link_url, local_url, download_url, description, type, date_time, tags) \
            VALUES ('%s', '%s', '%s', '%s', '%s', '%s','%s', '%s', '%s')" % \
            (item['title'], item['img_url'], item['link_url'], image_paths[0][5:], item['download_url'], item['description'], item['type'], int(round(time.time() * 1000)), item['tags'])
        try:
        # 执行sql语句
            cursor.execute(sql)
            # 提交到数据库执行
            db.commit()
        except:
        # 如果发生错误则回滚
            db.rollback()
        print(results, image_paths, '---------------663')
        if not image_paths:
            raise DropItem("Item contains no images")
        item['image_paths'] = image_paths
        return item