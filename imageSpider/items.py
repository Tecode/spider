# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# https://doc.scrapy.org/en/latest/topics/items.html

import scrapy

# // 爬取封面图片
class ImagespiderItem(scrapy.Item):
    # define the fields for your item here like:
    # name = scrapy.Field()
    title = scrapy.Field()
    img_url = scrapy.Field()
    link_url = scrapy.Field()
# // 爬取详情
class ImagesDetailItem(scrapy.Item):
    title = scrapy.Field()
    img_url = scrapy.Field()
    link_url = scrapy.Field()

# 爬取站长素材的PSD
class FreePsdItem(scrapy.Item):
    # define the fields for your item here like:
    # name = scrapy.Field()
    title = scrapy.Field()
    img_url = scrapy.Field()
    link_url = scrapy.Field()
    download_url = scrapy.Field()
    detail_img = scrapy.Field()
    image_paths = scrapy.Field()
    description = scrapy.Field()
    type = scrapy.Field()

# 爬取站长素材的html
class HtmlItem(scrapy.Item):
    title = scrapy.Field()
    img_url = scrapy.Field()
    link_url = scrapy.Field()
    download_url = scrapy.Field()
    detail_img = scrapy.Field()
    image_paths = scrapy.Field()
    description = scrapy.Field()
    type = scrapy.Field()
    tags = scrapy.Field()