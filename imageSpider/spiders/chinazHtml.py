# -*- coding: utf-8 -*-
import scrapy
from imageSpider.items import HtmlItem


class ChinazhtmlSpider(scrapy.Spider):
    name = 'chinazHtml'
    allowed_domains = ['sc.chinaz.com']
    start_urls = ['http://sc.chinaz.com/moban/index.html']
    base_url = 'http://sc.chinaz.com/moban/'
    offset = 1

    def parse(self, response):
        nodes = response.xpath('//div[@class="box picblock col3"]')
        for node in nodes:
            item = HtmlItem()
            item['title'] = node.xpath('./p/a[1]/text()').extract()[0]
            item['img_url'] = node.xpath('./div/a[1]/img/@src').extract()[0]
            link_url = node.xpath('./div/a[1]/@href').extract()[0]
            item['link_url'] = node.xpath('./p/a[2]/@href').extract()[0]
            if (link_url and link_url.find('http://') > -1):
                yield scrapy.Request(link_url, meta={'item':item}, callback=self.detail)

        if self.offset < 1:
            self.offset += 1
            url = self.base_url + 'index_' + str(self.offset) + '.html'
            yield scrapy.Request(url, callback=self.parse)

    def detail(self, response):
        item = response.meta['item'];
        item['detail_img'] = response.xpath('//div[@class="imga"]/a/img/@src').extract()[0];
        item['type'] = response.xpath('//div[@class="down_img"]/div/div/span[2]/a/text()').extract()[0];
        item['description'] = response.xpath('//div[@class="gjz_tag"]/div[2]/div/text()').extract()[0];
        tagsNode = response.xpath('//div[@class="gjz_tag"]/div/div/a');
        hrefs = response.xpath('//div[@class="downcon"]/div/div[3]/a');
        url = '';
        tags = '';
        for href in hrefs:
            url += href.xpath('./@href').extract()[0] + ',';
        item['download_url'] = url;
        for tag in tagsNode:
            tags += tag.xpath('text()').extract()[0] + ',';
        item['download_url'] = url;
        item['tags'] = tags;
        yield item;
