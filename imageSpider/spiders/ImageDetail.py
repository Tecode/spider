# -*- coding: utf-8 -*-
import scrapy
from imageSpider.items import ImagesDetailItem


class ImagedetailSpider(scrapy.Spider):
    name = 'imageDetail'
    allowed_domains = ['www.mm131.com']
    base_url = 'http://www.mm131.com/xinggan/'
    start_urls = [
        'http://www.mm131.com/xinggan/2892.html', 
        'http://www.mm131.com/xinggan/2312.html',
        'http://www.mm131.com/xinggan/2372.html',
        'http://www.mm131.com/xinggan/2613.html'
        'http://www.mm131.com/xinggan/2668.html']
    request_url = ''

    def parse(self, response):
        nodes = response.xpath(
            '//div[@class="content"]')
        for node in nodes:
            item = ImagesDetailItem()
            link_url = node.xpath('./div[@class="content-pic"]/a/@href').extract()[0]
            item['title'] = node.xpath('./h5/text()').extract()[0]
            item['img_url'] = node.xpath('./div/a/img[1]/@src').extract()[0]
            if (link_url and link_url.find('http://') > -1):
                self.request_url = link_url
                item['link_url'] = link_url
            else:
                self.request_url = self.base_url + link_url 
                item['link_url'] = self.base_url + link_url 
            yield item

        if self.request_url:
            yield scrapy.Request(self.request_url, callback=self.parse)
        